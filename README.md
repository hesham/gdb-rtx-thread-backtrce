 What this script does is to make use of GDB's Python to get the backtrace of all the threads running on the system.
 
 How it does this:
 1. Use OS structure, os_active_TCB, the array of thread control blocks (You have to know the number of threads, here, it's 20)
 2. From the thread control block identify where is the different thread stacks are.
 3. Read from the stack all the vital registers; SP, LR, and PC
 4. Save these registers for the current, running thread.
 5. Loop over the different threads, change the vital registers to the ones matching the thread, then print the backtrace.
 6. Enjoy a poor-man's OS-aware debugger.