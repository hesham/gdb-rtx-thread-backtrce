#!/usr/bin/env python

import gdb

THREAD_COUNT = 20
PC_STACK_OFFSET = 14        # in words, which is 14 x 4 bytes

TASK_STATE = ['INACTIVE', 'READY', 'RUNNING', 'WAIT_DLY', 'WAIT_ITV', 'WAIT_OR', 'WAIT_AND', 'WAIT_SEM', 'WAIT_MBX', 'WAIT_MUT' ]
STATE_READY   = 1
STATE_RUNNING = 2

longType = gdb.lookup_type("long")

all_tasks = gdb.parse_and_eval('(struct OS_TCB *[]) os_active_TCB')

state = [long(all_tasks[i]['state']) for i in range(THREAD_COUNT)]

prio = [long(all_tasks[i]['prio']) for i in range(THREAD_COUNT)]

stacks = [long(all_tasks[i]['tsk_stack']) for i in range(THREAD_COUNT)]

progCounters = [(all_tasks[i]['tsk_stack'].cast(longType.pointer()) + PC_STACK_OFFSET).dereference() for i in range(THREAD_COUNT)]
threadLR = [(all_tasks[i]['tsk_stack'].cast(longType.pointer()) + PC_STACK_OFFSET - 1).dereference() for i in range(THREAD_COUNT)]

PC = gdb.parse_and_eval('$pc')
SP = gdb.parse_and_eval('$sp')
LR = gdb.parse_and_eval('$lr')

IDLE_INDEX = 0
runnigIndex = state.index(STATE_RUNNING)

# Adjust stacks of the running thread

stackPtrs = [stacks[i] + (4*16) for i in range(THREAD_COUNT)]

#print "=========================================================\n"
print "[0] IDLE_THREAD   (%s)\n" % TASK_STATE[state[0]]
print "=========================================================\n"

for i in range(1,THREAD_COUNT):
    if i != runnigIndex:
        gdb.parse_and_eval("$pc=%d" % (progCounters[i]))
        gdb.parse_and_eval("$sp=%d" % (stackPtrs[i]))
        gdb.parse_and_eval("$lr=%d" % (threadLR[i]))
         
    else:
#        gdb.execute("set $pc=%d" % (PC))
#        gdb.execute("set $sp=%d" % (SP))
        gdb.parse_and_eval("$pc=%d" % (PC))
        gdb.parse_and_eval("$sp=%d" % (SP))
        gdb.parse_and_eval("$lr=%d" % (LR))
    print "[%d]  (%s)\n" % (i, TASK_STATE[state[i]])
    gdb.execute("bt");
#    frame = gdb.newest_frame()
#    while frame != None:
#        print "%x %s %s " %(frame.pc(), frame.function(), frame.find_sal())
#        frame = frame.older()
    print "=========================================================\n"

# Restore LR, SP, and PC
gdb.parse_and_eval("$pc=%d" % (PC))
gdb.parse_and_eval("$sp=%d" % (SP))
gdb.parse_and_eval("$lr=%d" % (LR))